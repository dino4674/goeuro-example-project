//
//  GETestViewController.h
//  GoEuroExampleProject
//
//  Created by Dino Bartošak on 07/10/14.
//  Copyright (c) 2014 GoEuro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GETestViewController : UIViewController


@property (nonatomic) BOOL isAnimating;
@property (nonatomic) NSString *subjectText;

- (void)startAnimation;
- (void)stopAnimation;

@end
