//
//  LGScrollMenuViewController.h
//  Looks Good
//
//  Created by Dino Bartošak on 19/09/14.
//  Copyright (c) 2014 Dino Bartosak. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGScrollMenuViewControllerDelegate;

@interface LGScrollMenuViewController : UIViewController

@property (nonatomic, weak) id <LGScrollMenuViewControllerDelegate> delegate;

@property (nonatomic, readonly) NSArray *viewControllers;

@property (nonatomic) NSUInteger activeViewControllerIndex;
@property (nonatomic) BOOL scrollEnabled;
@property (nonatomic, readonly) BOOL isDragging;
@property (nonatomic, readonly) UIPanGestureRecognizer *scrollPanRecognizer;

@property (nonatomic, readonly) CGFloat horizontalTranslation;
@property (nonatomic, readonly) CGFloat relativeOffset;


- (id)initWithViewControllers:(NSArray *)viewControllers;

- (void)setActiveViewControllerIndex:(NSUInteger)activeViewControllerIndex
                            animated:(BOOL)animated
                          completion:(void(^)(BOOL finished))completion;
@end

@protocol LGScrollMenuViewControllerDelegate <NSObject>

- (void)scrollMenuViewControllerDidTranslateHorizontaly:(UIViewController *)controller;

@end
