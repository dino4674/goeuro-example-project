//
//  GEViewController.m
//  GoEuroExampleProject
//
//  Created by Dino Bartošak on 07/10/14.
//  Copyright (c) 2014 GoEuro. All rights reserved.
//

#import "GEViewController.h"
#import "LGScrollMenuViewController.h"
#import "LGHeaderTitleLabelView.h"
#import "GETestViewController.h"

#import "UIDefines.h"

@interface GEViewController () <LGScrollMenuViewControllerDelegate>

@property (nonatomic) LGScrollMenuViewController *scrollMenu;
@property (nonatomic) LGHeaderTitleLabelView *headerTitleView;

@property (nonatomic) BOOL headerVisible;

@end

@implementation GEViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSUInteger controllersCount = 3;
    
    // set controllers
    NSArray *controllers = [self createTestViewControllers:controllersCount];

    _scrollMenu = [[LGScrollMenuViewController alloc] initWithViewControllers:controllers];
    _scrollMenu.view.frame = self.view.bounds;
    _scrollMenu.delegate = self;
    
    [self.view addSubview:_scrollMenu.view];
    [self addChildViewController:_scrollMenu];
    
    // set titles
    NSArray *titles = [self createTestTitles:controllersCount];

    CGRect bounds = self.view.bounds;
    CGRect headerFrame = CGRectMake(0, 0, bounds.size.width, 80);
    _headerTitleView = [[LGHeaderTitleLabelView alloc] initWithFrame:headerFrame];
    _headerTitleView.titles = titles;
    _headerTitleView.backgroundColor = RGBA(0, 0, 0, 0.6);

    _headerVisible = YES;
    
    [self.view addSubview:_headerTitleView];
    
    // create tap gesture recognizer
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)];
    [self.view addGestureRecognizer:tap];
    
    [self updateMenu];
}

- (void)viewDidTap:(UITapGestureRecognizer *)tap
{
    [self setHeaderVisible:!_headerVisible animated:YES];
}

- (void)setHeaderVisible:(BOOL)visible animated:(BOOL)animated
{
    _headerVisible = visible;
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    if (!_headerVisible) {
        transform = CGAffineTransformMakeTranslation(0, -_headerTitleView.frame.size.height);
    }
    
    if (animated) {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:0
                         animations:^{
                            _headerTitleView.transform = transform;
                         }
                         completion:nil];
    } else {
        _headerTitleView.transform = transform;
    }
}

#pragma mark - Private

- (void)updateMenu
{
    // update header

    CGFloat scrollWidth = _scrollMenu.view.frame.size.width;
    CGFloat relativeOffset = _scrollMenu.relativeOffset;
    
    NSUInteger activeIndex = _scrollMenu.activeViewControllerIndex;
    
    [_headerTitleView setTransformForLeftIndex:activeIndex
                           relativeOffsetRatio:relativeOffset/scrollWidth];
}

#pragma mark - Scroll Menu Delegate

- (void)scrollMenuViewControllerDidTranslateHorizontaly:(UIViewController *)controller
{
    [self updateMenu];
}


#pragma mark - Test Title Factory

- (NSArray *)createTestTitles:(NSUInteger)titlesCount
{
    NSMutableArray *titles = [NSMutableArray arrayWithCapacity:titlesCount];
    
    for (int i=0; i<titlesCount; i++) {
        [titles addObject:[NSString stringWithFormat:@"I am controller #%@", @(i)]];
    }
    
    return titles;
}

#pragma mark - Test Controller Factory

- (NSArray *)createTestViewControllers:(NSUInteger)controllersCount
{
    NSMutableArray *controllers = [NSMutableArray arrayWithCapacity:controllersCount];
    
    for (int i=0; i<controllersCount; i++) {
        UIViewController *testController = [self testControllerWithIndex:i];
        [controllers addObject:testController];
    }
    
    return controllers;
}

- (UIViewController *)testControllerWithIndex:(NSUInteger)index
{
    GETestViewController *testController = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_test"];
    
    testController.subjectText = [NSString stringWithFormat:@"#%@", @(index)];
    
    return testController;
}

@end
