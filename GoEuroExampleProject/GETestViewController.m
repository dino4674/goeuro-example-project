//
//  GETestViewController.m
//  GoEuroExampleProject
//
//  Created by Dino Bartošak on 07/10/14.
//  Copyright (c) 2014 GoEuro. All rights reserved.
//

#import "GETestViewController.h"

#import "UIDefines.h"

@interface GETestViewController ()

@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (nonatomic) NSString *subjectLabelText;

@property (nonatomic) NSMutableArray *forceViews;

@end

@implementation GETestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self refreshSubjectLabelText];
    
    _forceViews = [NSMutableArray array];
    
    [self startAnimation];
    
    self.view.clipsToBounds = YES;
}

- (CGPoint)ballCenter
{
    return CGPointMake(120, 333); // use iPhone 5 for now
}

- (void)refreshSubjectLabelText
{
    _subjectLabel.text = _subjectLabelText;
}

- (void)addPulseViewWithRadius:(CGFloat)radius duration:(NSTimeInterval)duration
{
    UIView *view = [self addForceViewWithRadius:radius];
    [self addPulseAnimationToView:view duration:duration];
    
    [_forceViews addObject:view];
}

- (void)addExpandViewWithRadius:(CGFloat)radius duration:(NSTimeInterval)duration delay:(NSTimeInterval)delay
{
    UIView *view = [self addForceViewWithRadius:radius];
    [self addExpandAnimationToView:view duration:duration delay:delay];

    [_forceViews addObject:view];
}

#pragma mark - Force View Factory

- (UIView *)addForceViewWithRadius:(CGFloat)radius
{
    UIView *forceView = [self forceViewWithRadius:radius center:[self ballCenter]];
    [self.view addSubview:forceView];
    
    return forceView;
}

- (UIView *)forceViewWithRadius:(CGFloat)radius center:(CGPoint)center
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];

    view.frame = CGRectMake(center.x - radius,
                            center.y - radius,
                            2*radius,
                            2*radius);
    view.clipsToBounds = YES;
    
    view.layer.borderWidth = 0.5;
    view.layer.borderColor = [RGBA(255, 255, 255, 0.2) CGColor];
    
    view.layer.cornerRadius = radius;
    
    [self.view addSubview:view];

    return view;
}

#pragma mark - Animation

- (void)addExpandAnimationToView:(UIView *)view duration:(NSTimeInterval)duration delay:(NSTimeInterval)delay
{
    CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulseAnimation.duration = duration;
    pulseAnimation.repeatCount = HUGE_VAL;
    pulseAnimation.autoreverses = NO;
    pulseAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    pulseAnimation.toValue = [NSNumber numberWithFloat:9.0];
    
    pulseAnimation.beginTime = CACurrentMediaTime()+delay;

    [view.layer addAnimation:pulseAnimation forKey:@"scale"];
}

- (void)addPulseAnimationToView:(UIView *)view duration:(NSTimeInterval)duration
{
    CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulseAnimation.duration = duration;
    pulseAnimation.repeatCount = HUGE_VAL;
    pulseAnimation.autoreverses = YES;
    pulseAnimation.fromValue = [NSNumber numberWithFloat:1.2];
    pulseAnimation.toValue = [NSNumber numberWithFloat:0.8];
    
    [view.layer addAnimation:pulseAnimation forKey:@"scale"];
}

#pragma mark - Public API

- (NSString *)subjectText
{
    return _subjectLabel.text;
}

- (void)setSubjectText:(NSString *)subjectText
{
    _subjectLabelText = subjectText;
    
    [self refreshSubjectLabelText];
}

- (void)startAnimation
{
    if (!_isAnimating) {
        _isAnimating = YES;
        
        // play with this
        [self addPulseViewWithRadius:45 duration:0.2];
        [self addPulseViewWithRadius:80 duration:0.5];
        [self addPulseViewWithRadius:90 duration:0.9];
        
        NSTimeInterval duration = 3.0;
        [self addExpandViewWithRadius:45 duration:duration delay:0];
//        [self addExpandViewWithRadius:45 duration:duration delay:0.4];
//        [self addExpandViewWithRadius:45 duration:duration delay:0.8];
//        [self addExpandViewWithRadius:45 duration:duration delay:1.2];
//        [self addExpandViewWithRadius:45 duration:duration delay:1.6];
//        [self addExpandViewWithRadius:45 duration:duration delay:2.0];
//        [self addExpandViewWithRadius:45 duration:duration delay:2.4];
    }
}

- (void)stopAnimation
{
    if (_isAnimating) {
        _isAnimating = NO;
        
        for (UIView *view in _forceViews) {
            [view removeFromSuperview];
        }
        
        [_forceViews removeAllObjects];
    }
}

@end
