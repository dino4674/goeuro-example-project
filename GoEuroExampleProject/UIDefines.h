//
//  UIDefines.h
//  GoEuroExampleProject
//
//  Created by Dino Bartošak on 07/10/14.
//  Copyright (c) 2014 GoEuro. All rights reserved.
//


#define RGB(r, g, b) [UIColor colorWithRed:(float)r / 255.0 green:(float)g / 255.0 blue:(float)b / 255.0 alpha:1.0]
#define RGBA(r, g, b, a) [UIColor colorWithRed:(float)r / 255.0 green:(float)g / 255.0 blue:(float)b / 255.0 alpha:a]
