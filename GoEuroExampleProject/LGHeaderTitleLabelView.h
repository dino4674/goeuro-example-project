//
//  LGHeaderTitleLabelView.h
//  Looks Good
//
//  Created by Dino Bartošak on 20/09/14.
//  Copyright (c) 2014 Dino Bartosak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LGHeaderTitleLabelView : UIView

@property (nonatomic, readonly) NSUInteger leftIndex;
@property (nonatomic, readonly) CGFloat relativeOffsetRatio;

@property (nonatomic) NSArray *titles;

- (void)setTransformForLeftIndex:(NSUInteger)leftIndex relativeOffsetRatio:(CGFloat)relativeOffsetRatio; // [0..1]

@end
