//
//  LGHeaderTitleLabelView.m
//  Looks Good
//
//  Created by Dino Bartošak on 20/09/14.
//  Copyright (c) 2014 Dino Bartosak. All rights reserved.
//

#import "LGHeaderTitleLabelView.h"
#import "UIDefines.h"

@interface LGHeaderTitleLabelView ()

@property (nonatomic) UILabel *leftLabel;
@property (nonatomic) UILabel *rightLabel;

@end

@implementation LGHeaderTitleLabelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initLabels];    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self initLabels];
    }
    
    return self;
}

- (void)initLabels
{
    _leftLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _rightLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    
    NSString *fontName = @"HelveticaNeue-Light";
    UIFont *font = [UIFont fontWithName:fontName size:28];
    
    UIColor *color = [UIColor whiteColor];
    
    _leftLabel.font = font;
    _rightLabel.font = font;
    
    _leftLabel.textColor = color;
    _rightLabel.textColor = color;
    
    [self addSubview:_leftLabel];
    [self addSubview:_rightLabel];
    
    
    // debug
    //    self.backgroundColor = RGBA(100, 100, 100, 0.5);
    //    _leftLabel.backgroundColor = RGBA(100, 100, 100, 0.2);
    //    _rightLabel.backgroundColor = RGBA(100, 100, 100, 0.8);
}

- (void)layoutSubviews
{
    [self updateLabelsTransform];
}

- (void)setTransformForLeftIndex:(NSUInteger)leftIndex relativeOffsetRatio:(CGFloat)relativeOffsetRatio
{
    NSAssert(leftIndex < _titles.count, @"nono");
    
    _leftIndex = leftIndex;
    _relativeOffsetRatio = relativeOffsetRatio;
    
    [self updateLabelsTransform];
}

#pragma mark - Private

- (void)setLabelTextAndPositionToCenter:(UILabel *)label text:(NSString *)text animate:(BOOL)animate
{
    CGPoint center = CGPointMake(self.bounds.size.width / 2.0,
                                 self.bounds.size.height / 2.0);
    
    UILabel *animationLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    animationLabel.text = label.text;
    animationLabel.font = label.font;
    animationLabel.textColor = label.textColor;
    [animationLabel sizeToFit];
    animationLabel.center = center;
    
    label.transform = CGAffineTransformIdentity;
    label.text = text;
    [label sizeToFit];
    label.center = center;
    
    // start animation
    
    if (animate) {
        //    animationLabel.transform = oldTransform;
        
        label.alpha = 0.0;
        [self addSubview:animationLabel];
        [UIView animateWithDuration:0.3
                              delay:0
                            options:0
                         animations:^{
                             animationLabel.alpha = 0.0;
                             label.alpha = 1.0;
                         }
                         completion:^(BOOL finished) {
                             [animationLabel removeFromSuperview];
                         }];
    }
}

- (void)updateLabelsTransform
{
    NSString *leftTitle = nil;
    NSString *rightTitle = nil;
    
    leftTitle = _titles[_leftIndex];
    if (_leftIndex < _titles.count-1) {
        rightTitle = _titles[_leftIndex + 1];
    }
    
    
    if (![_leftLabel.text isEqualToString:leftTitle]) {
        [self setLabelTextAndPositionToCenter:_leftLabel text:leftTitle animate:NO];
    }
    
    if (rightTitle) {
        if (![_rightLabel.text isEqualToString:rightTitle]) {
            [self setLabelTextAndPositionToCenter:_rightLabel text:rightTitle animate:NO];
        }
    } else {
        _rightLabel.text = nil;
    }
    
    // translation
    CGFloat leftTranslateMax = _leftLabel.bounds.size.width/2.0 + 50;
    CGFloat rightTranslateMax = _rightLabel.bounds.size.width/2.0 + 50;
    
    CGAffineTransform leftTitleTransform = CGAffineTransformMakeTranslation(-leftTranslateMax * _relativeOffsetRatio, 0);
    CGAffineTransform rightTitileTransform = CGAffineTransformMakeTranslation(rightTranslateMax - rightTranslateMax * _relativeOffsetRatio, 0);
    
    _leftLabel.transform = leftTitleTransform;
    _rightLabel.transform = rightTitileTransform;
    
    // alpha
    CGFloat quickerOffset = _relativeOffsetRatio;
    if (quickerOffset > 1.0) {
        quickerOffset = 1.0;
    }
    
    CGFloat leftAlpha = 1 - (1*quickerOffset);
    CGFloat rightAlpa = 1 - leftAlpha;
    
    if (leftAlpha < 0) leftAlpha = 0;
    if (leftAlpha > 1) leftAlpha = 1;
    if (rightAlpa < 0) rightAlpa = 0;
    if (rightAlpa > 1) rightAlpa = 1;
    
    _leftLabel.alpha = leftAlpha;
    _rightLabel.alpha = rightAlpa;
}

@end
