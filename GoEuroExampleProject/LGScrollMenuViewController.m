//
//  LGScrollMenuViewController.m
//  Looks Good
//
//  Created by Dino Bartošak on 19/09/14.
//  Copyright (c) 2014 Dino Bartosak. All rights reserved.
//

#import "LGScrollMenuViewController.h"
#import "UIDefines.h"

@interface LGScrollMenuViewController () <UIScrollViewDelegate>

@property (nonatomic) UIScrollView *containerScrollView;

@property (nonatomic) UIView *leftOverlayView;
@property (nonatomic) UIView *rightOverlayView;

@end

@implementation LGScrollMenuViewController

- (id)initWithViewControllers:(NSArray *)viewControllers
{
    if (self = [super init]) {
        _viewControllers = viewControllers;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUInteger viewControllersCount = _viewControllers.count;
    CGRect viewBounds = self.view.bounds;
    
    // load container scroll view
    _containerScrollView = [[UIScrollView alloc] init];
    _containerScrollView.frame = viewBounds;
    
    CGSize scrollContentSize = CGSizeMake(viewBounds.size.width * viewControllersCount,
                                          viewBounds.size.height);
    
    _containerScrollView.contentSize = scrollContentSize;
    _containerScrollView.pagingEnabled = YES;
    _containerScrollView.bounces = NO;
    _containerScrollView.showsHorizontalScrollIndicator = NO;
    _containerScrollView.delegate = self;
    
    [self.view addSubview:_containerScrollView];
    
    // layout view controllers
    for (int i=0; i<viewControllersCount; i++) {
        UIViewController *controller = _viewControllers[i];
        controller.view.frame = CGRectMake(viewBounds.origin.x + (i * viewBounds.size.width),
                                           viewBounds.origin.y,
                                           viewBounds.size.width,
                                           viewBounds.size.height);
        [_containerScrollView addSubview:controller.view];
        [self addChildViewController:controller];
    }
    
    // init overlay black views
    UIColor *color = RGB(0, 0, 0);
    _leftOverlayView = [self overlayDarkViewWithFrame:self.view.bounds backgroundColor:color];
    _rightOverlayView = [self overlayDarkViewWithFrame:self.view.bounds backgroundColor:color];
   
    [self.view addSubview:_leftOverlayView];
    [self.view addSubview:_rightOverlayView];

    [self updateOverlayDarkViews];
}

#pragma mark - Public API

- (void)setActiveViewControllerIndex:(NSUInteger)activeViewControllerIndex
{
    NSAssert(activeViewControllerIndex < _viewControllers.count, @"nope");

    CGRect viewBounds = self.view.bounds;
    CGPoint contentOffset = CGPointMake(viewBounds.size.width * activeViewControllerIndex, 0);
    _activeViewControllerIndex = activeViewControllerIndex;

    _containerScrollView.contentOffset = contentOffset;
}

- (void)setActiveViewControllerIndex:(NSUInteger)activeViewControllerIndex
                            animated:(BOOL)animated
                          completion:(void(^)(BOOL finished))completion
{
    NSAssert(activeViewControllerIndex < _viewControllers.count, @"nope");

    [UIView animateWithDuration:0.2
                          delay:0
                        options:0
                     animations:^{
                         [self setActiveViewControllerIndex: activeViewControllerIndex];
                     }
                     completion:completion];
}

- (void)setScrollEnabled:(BOOL)scrollEnabled
{
    _containerScrollView.scrollEnabled = scrollEnabled;
}

- (BOOL)scrollEnabled
{
    return _containerScrollView.scrollEnabled;
}

- (BOOL)isDragging
{
    return _containerScrollView.isDragging;
}

- (CGFloat)horizontalTranslation
{
    return _containerScrollView.contentOffset.x;
}

- (CGFloat)relativeOffset
{
    CGFloat offset = _containerScrollView.contentOffset.x;
    CGFloat width = _containerScrollView.bounds.size.width;
    
    CGFloat relativeOffset = offset -  (NSInteger)(offset / width) * width; // offset % width --> loses precision in integer conversion
    
    return relativeOffset;
}

- (UIPanGestureRecognizer *)scrollPanRecognizer
{
    return _containerScrollView.panGestureRecognizer;
}

#pragma mark - Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect viewBounds = self.view.bounds;
    CGFloat offset = _containerScrollView.contentOffset.x;

    NSUInteger index = (NSUInteger)(offset / viewBounds.size.width);
    _activeViewControllerIndex = index;
    
    [_delegate scrollMenuViewControllerDidTranslateHorizontaly:self];
    
    [self updateOverlayDarkViews];
}

#pragma mark - Overlay Dark Views

- (UIView *)overlayDarkViewWithFrame:(CGRect)frame
                     backgroundColor:(UIColor *)backgroundColor
{
    UIView *overlayView = [[UIView alloc] initWithFrame:frame];
    overlayView.backgroundColor = backgroundColor;
    overlayView.userInteractionEnabled = NO;
    
    overlayView.alpha = 0.0;
    
    return overlayView;
}

- (void)updateOverlayDarkViews
{
    // update frames
    CGFloat relativeOffset = [self relativeOffset];
    CGRect bounds = self.view.bounds;
    
    CGRect leftFrame = CGRectMake(bounds.origin.x - relativeOffset,
                                  0,
                                  bounds.size.width,
                                  bounds.size.height);
    CGRect rightFrame = CGRectMake(leftFrame.origin.x + bounds.size.width,
                                  0,
                                  bounds.size.width,
                                  bounds.size.height);
    _leftOverlayView.frame = leftFrame;
    _rightOverlayView.frame = rightFrame;
    
    // update alpha
    CGFloat visibleAlpha = 0.0;
    CGFloat hiddenAlpha = 1.0;
    
    CGFloat alphaRatio = (relativeOffset) / bounds.size.width;
    CGFloat leftAlpha = (hiddenAlpha - visibleAlpha) * alphaRatio + visibleAlpha;
    CGFloat rightAlpha = (hiddenAlpha - visibleAlpha) * (1 - alphaRatio) + visibleAlpha;
    
    _leftOverlayView.alpha = leftAlpha;
    _rightOverlayView.alpha = rightAlpha;
}

@end
