//
//  GEAppDelegate.h
//  GoEuroExampleProject
//
//  Created by Dino Bartošak on 07/10/14.
//  Copyright (c) 2014 GoEuro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
