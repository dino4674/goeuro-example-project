//
//  main.m
//  GoEuroExampleProject
//
//  Created by Dino Bartošak on 07/10/14.
//  Copyright (c) 2014 GoEuro. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GEAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GEAppDelegate class]));
    }
}
